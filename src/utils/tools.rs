use crate::error::ConfigurationError;
use serde::Deserialize;
use std::fs::read_to_string;

// TODO: offer function to save a data dict to a confic toml
#[derive(Debug, Deserialize)]
pub struct Config {
    pub version: String,
    pub output_dimensions: [u32; 2],
    pub zoom_levels: u8,
    pub zoom_fraction_min: f64,
    pub zoom_fraction_max: f64,
}

impl Config {
    pub fn new(
        output_dimensions: [u32; 2],
        zoom_levels: u8,
        zoom_fraction_min: f64,
        zoom_fraction_max: f64,
    ) -> Self {
        Config {
            version: String::from("0.1"),
            output_dimensions,
            zoom_levels,
            zoom_fraction_min,
            zoom_fraction_max,
        }
    }
}

impl Default for Config {
    fn default() -> Self {
        Config {
            version: String::from("0.1"),
            output_dimensions: [256, 256],
            zoom_levels: 9,
            zoom_fraction_min: 0.05, // 4%
            zoom_fraction_max: 1.,   // 100%
        }
    }
}

/// Takes in a string and attempts to parse a configuration from the provided path
pub fn read_config(path: &String) -> Result<Config, Box<dyn std::error::Error>> {
    let config_str = read_to_string(path)?;

    // Transform toml deserialization error to config error
    toml::from_str(&config_str).map_err(|_| -> Box<dyn std::error::Error> {
        Box::new(ConfigurationError::with_description(String::from(
            "Could not parse config from given path.",
        )))
    })
}

// ConfigurationError::with_description("Could not parse config: {:?}", x)
pub fn serialize_config(config: &Config) -> String {
    format!("title = \"Celeste export config\"\nversion = {:?}\noutput_dimensions = [{}, {}]\nzoom_levels = {}\nzoom_fraction_min = {}\nzoom_fraction_max = {}", config.version, config.output_dimensions[0], config.output_dimensions[1], config.zoom_levels, config.zoom_fraction_min, config.zoom_fraction_max)
}
