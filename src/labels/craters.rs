use csv::ReaderBuilder;
use druid::{im::Vector, Data};
use geo::transformation::LatLong;
use serde::{Deserialize, Serialize};
use std::{error::Error, fs::read_to_string, io, path::PathBuf};

#[derive(Debug, Data, Default, Clone, Serialize, Deserialize)]
pub struct CraterData {
    pub crater_id: String,
    #[data(same_fn = "compare_latlongs")]
    pub coordinate: LatLong,
    pub diameter_m: f64,
    #[data(same_fn = "compare_latlongs")]
    pub coordinate_original: LatLong,
    pub secondary: bool,
    pub updated: bool,
    pub selected: bool,
    pub local_raster_coordinates: Option<[f64; 2]>,
    pub prediction_score: Option<f32>,
}

pub fn compare_latlongs(into: &LatLong, output: &LatLong) -> bool {
    into.lat == output.lat && into.long == output.long
}
// TODO: Also write a parser for the modified format!
pub fn export_modified_data(crater_data: &Vector<CraterData>) -> Result<(), Box<dyn Error>> {
    let data = crater_data.iter().filter(|&c| c.selected);
    let mut wtr = csv::WriterBuilder::new()
        .delimiter(b';')
        .from_writer(io::stdout());

    wtr.write_record(&[
        "crater_id",
        "latitude_deg",
        "longitude_deg",
        "diameter_m",
        "latitude_orig_deg",
        "longitude_orig_deg",
        "secondary",
        "updated",
        "selected",
    ])?;
    for c in data {
        wtr.write_record(&[
            c.crater_id.clone(),
            c.coordinate.lat.to_string(),
            c.coordinate.long.to_string(),
            c.diameter_m.to_string(),
            c.coordinate_original.lat.to_string(),
            c.coordinate_original.long.to_string(),
            c.secondary.to_string(),
            c.updated.to_string(),
            c.selected.to_string(),
        ])?;
    }
    wtr.flush()?;
    Ok(())
}

pub fn read_csv_predicted_crater_data(path: &str) -> Result<Vector<CraterData>, Box<dyn Error>> {
    println!("{}", path);

    let mut vec = Vector::new();

    let mut rdr = ReaderBuilder::new()
        .delimiter(b',')
        .has_headers(true)
        .from_path(path)?;
    for result in rdr.records() {
        let record = match result {
            Ok(value) => value,
            Err(_) => {
                continue;
            }
        };

        if record.is_empty() || record.len() < 4 {
            continue;
        }

        let latlong = LatLong::new(
            record.get(2).unwrap().parse::<f64>().unwrap(),
            record.get(3).unwrap().parse::<f64>().unwrap(),
        );
        let new_crater = CraterData {
            crater_id: String::from(record.get(1).unwrap()),
            coordinate: latlong,
            diameter_m: record.get(4).unwrap().parse::<f64>().unwrap(),
            coordinate_original: latlong,
            secondary: false,
            updated: false,
            selected: false,
            local_raster_coordinates: None,
            prediction_score: Some(record.get(5).unwrap().parse::<f32>().unwrap()),
        };
        vec.push_back(new_crater);
    }
    Ok(vec)
}

pub fn read_tsv_crater_data(path: &str) -> Result<Vector<CraterData>, Box<dyn Error>> {
    println!("{}", path);

    let mut vec = Vector::new();

    let mut rdr = ReaderBuilder::new()
        .delimiter(b'\t')
        .has_headers(true)
        .from_path(path)?;
    for result in rdr.records() {
        let record = match result {
            Ok(value) => value,
            Err(_) => {
                continue;
            }
        };

        if record.is_empty() || record.len() < 5 {
            continue;
        }

        let latlong = LatLong::new(
            record.get(1).unwrap().parse::<f64>().unwrap(),
            record.get(2).unwrap().parse::<f64>().unwrap(),
        );
        let secondary = match record.get(44) {
            Some(val) => {
                if val.eq_ignore_ascii_case("Y") {
                    true
                } else {
                    false
                }
            }
            None => false,
        };
        let new_crater = CraterData {
            crater_id: String::from(record.get(0).unwrap()),
            coordinate: latlong,
            diameter_m: record.get(5).unwrap().parse::<f64>().unwrap() * 1000.,
            coordinate_original: latlong,
            secondary,
            updated: false,
            selected: false,
            local_raster_coordinates: None,
            prediction_score: None,
        };
        vec.push_back(new_crater);
    }
    Ok(vec)
}

/// There are several different types of diam crater files:
///     crater = {diameter
///     crater = {diameter, fraction
///     crater = {diameter, fraction, lon, lat
///     crater = {diameter, fraction, lon, lat, topo_scale_factor
///     Since the first two formats do not include the coordinates of the
///     crater, these will not be parsed.

pub fn read_diam_crater_data(path: &str) -> Result<Vector<CraterData>, Box<dyn Error>> {
    let path = PathBuf::from(path);
    let crater_data = read_to_string(&path)?;
    let mut data_slice: std::str::Split<&[char]> = crater_data.split(&['{', '}'][..]);
    data_slice.next();
    let data = match data_slice.next() {
        Some(value) => value,
        None => {
            return Err(Box::new(io::Error::new(
                io::ErrorKind::InvalidData,
                "Could not read out the tab-separated-data inside curly brackets of diam file.",
            )))
        }
    };

    let mut output_vec = Vector::new();

    let mut rdr = ReaderBuilder::new()
        .delimiter(b'\t')
        // .has_headers(true)
        .flexible(true)
        .from_reader(data.as_bytes());

    let id_prefix = String::from(path.file_name().unwrap().to_str().unwrap());
    let mut counter = 0;
    for result in rdr.records() {
        let record = match result {
            Ok(value) => value,
            Err(r) => {
                println!("there was an error: {:?}", r);
                continue;
            }
        };

        if record.is_empty() || record.len() < 4 {
            println!("Skipping record: {:?}", record);
            continue;
        }

        let latlong = LatLong::new(
            record.get(3).unwrap().parse::<f64>().unwrap(),
            record.get(2).unwrap().parse::<f64>().unwrap(),
        );
        let new_crater = CraterData {
            crater_id: format!("{}_{}", id_prefix, counter),
            coordinate: latlong,
            diameter_m: record.get(0).unwrap().parse::<f64>().unwrap() * 1000.,
            coordinate_original: latlong,
            secondary: false,
            updated: false,
            selected: false,
            local_raster_coordinates: None,
            prediction_score: None,
        };
        output_vec.push_back(new_crater);
        counter += 1;
    }
    Ok(output_vec)
}
