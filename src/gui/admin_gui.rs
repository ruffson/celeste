use std::sync::Arc;

use crate::image::ImageData;
use crate::utils::tools;
use crate::{
    gui::viewport::{Image, ImageSliceConfig, ImageViewer, ViewerMode},
    labels::craters,
    utils::tools::Config,
};
use druid::{
    commands::{self, SHOW_SAVE_PANEL},
    text::format::ParseFormatter,
    widget::{Align, Flex, Slider, TextBox, WidgetExt},
    widget::{Button, Label},
    AppDelegate, AppLauncher, Color, Command, Data, DelegateCtx, Env, FileDialogOptions, FileSpec,
    Handled, LocalizedString, MenuDesc, MenuItem, Selector, Target, Widget, WindowDesc,
};
const MENU_CLEAR_CRATER: Selector<usize> = Selector::new("menu-clear-crater-action");
const MENU_EXPORT_CRATER: Selector<usize> = Selector::new("menu-export-crater-data");
const MENU_SELECT_ALL_CRATERS: Selector<usize> = Selector::new("menu-select-all-craters");
const MENU_DESELECT_ALL_CRATERS: Selector<usize> = Selector::new("menu-deselect-all-craters");
const MENU_TOGGLE_CROP_VIEW: Selector<usize> = Selector::new("menu-toggle-crop-view");
const MENU_TOGGLE_SECONDARY_SHOW: Selector<usize> = Selector::new("menu-toggle-secondary_show");
const MENU_SELECT_ALL_EXPORT: Selector<usize> = Selector::new("menu-select-all-export");
const MENU_DESELECT_ALL_EXPORT: Selector<usize> = Selector::new("menu-deselect-all-export");

pub const MENU_IMAGE_EXPORT_CURRENT_TRAINING: Selector<usize> =
    Selector::new("menu-image-export-current-training");
pub const MENU_IMAGE_EXPORT_ALL_TRAINING: Selector<usize> =
    Selector::new("menu-image-export-all-training");
pub const MENU_IMAGE_EXPORT_CURRENT: Selector<usize> = Selector::new("menu-image-export-current");
pub const MENU_IMAGE_EXPORT_ALL: Selector<usize> = Selector::new("menu-image-export-all");
pub const CURRENT_ZOOM_LEVEL_CHANGED: Selector<usize> = Selector::new("current-zoom-level-changed");

struct Delegate;

pub fn start_admin_gui(config: Config) {
    let data = ImageViewer::with_config(config);
    let main_window = WindowDesc::new(ui_builder())
        .menu(make_menu())
        .window_size((900., 700.))
        .title("Celeste");
    AppLauncher::with_window(main_window)
        .delegate(Delegate)
        .launch(data)
        .expect("launch failed");
}

fn ui_builder() -> impl Widget<ImageViewer> {
    // ######
    // ClipBox wrapped image
    // ######

    let image_data = ImageData::default();

    let img = Image::new(image_data).background(Color::WHITE).center();

    // ######
    // Button bar
    // ######
    let tiff = FileSpec::new("Text file", &["tif", "tiff", "TIF", "TIFF"]);
    let open_dialog_options = FileDialogOptions::new()
        .allowed_types(vec![tiff])
        .default_type(tiff)
        .default_name("")
        .name_label("Source")
        .title("Choose a tiff image file.")
        .button_text("Import");

    let file_chooser = Button::new("Load").on_click(move |ctx, _, _| {
        ctx.submit_command(Command::new(
            druid::commands::SHOW_OPEN_PANEL,
            open_dialog_options.clone(),
            Target::Auto,
        ))
    });

    // ///////////////
    //Left side button bar
    // ///////////////
    let edit_button = Button::dynamic(|data: &ImageViewer, _| {
        let button_label = if data.viewer_mode == ViewerMode::CraterEdit {
            "Done"
        } else {
            "[E]dit Crater"
        };
        format!("{}", button_label)
    })
    .on_click(move |_, data: &mut ImageViewer, _| {
        if data.image_path.is_some() && data.crater_data.is_some() {
            // TODO: check how this works together with crater selection mode
            if data.viewer_mode == ViewerMode::CraterEdit {
                data.viewer_mode = ViewerMode::Navigation;
            } else {
                data.viewer_mode = ViewerMode::CraterEdit;
            }
        }
    });
    let crater_selection_button = Button::dynamic(|data: &ImageViewer, _| {
        let button_label = if data.viewer_mode == ViewerMode::CraterSelection {
            "Done"
        } else {
            "[S]elect Crater"
        };
        format!("{}", button_label)
    })
    .on_click(move |_, data: &mut ImageViewer, _| {
        if data.image_path.is_some() && data.crater_data.is_some() {
            if data.viewer_mode == ViewerMode::CraterSelection {
                data.viewer_mode = ViewerMode::Navigation;
            } else {
                data.viewer_mode = ViewerMode::CraterSelection;
            }
        }
    });

    let export_selection_button = Button::dynamic(|data: &ImageViewer, _| {
        let button_label = if data.viewer_mode == ViewerMode::ExportSelection {
            "Done"
        } else {
            "E[x]port"
        };
        format!("{}", button_label)
    })
    .on_click(move |_, data: &mut ImageViewer, _| {
        if data.image_path.is_some() {
            if data.viewer_mode == ViewerMode::ExportSelection {
                data.viewer_mode = ViewerMode::Navigation;
            } else if !data.image_slice_config.hidden {
                data.viewer_mode = ViewerMode::ExportSelection;
            }
        }
    });

    // ///////////////
    //Right side button bar
    // ///////////////

    let decrease_crop_level = Button::new("+").on_click(|ctx, data: &mut ImageViewer, _| {
        if data.image_slice_config.current_level > 0 {
            data.image_slice_config.current_level -= 1;
            ctx.submit_command(CURRENT_ZOOM_LEVEL_CHANGED.with(0));
        }
    });
    let increase_crop_level = Button::new("-").on_click(|ctx, data: &mut ImageViewer, _| {
        if data.image_slice_config.current_level < data.image_slice_config.zoom_levels - 1 {
            data.image_slice_config.current_level += 1;
            ctx.submit_command(CURRENT_ZOOM_LEVEL_CHANGED.with(0));
        }
    });

    let button_bar = Flex::row()
        .with_flex_child(Align::left(file_chooser), 0.0)
        .with_flex_child(Align::left(edit_button), 0.0)
        .with_flex_child(Align::left(crater_selection_button), 0.0)
        .with_flex_child(Align::left(export_selection_button), 1.0)
        .with_flex_child(Align::right(Label::new("score:")), 0.0)
        .with_flex_child(
            Align::right(Slider::new().lens(ImageViewer::score_threshold)),
            0.,
        )
        .with_flex_child(
            Align::right(Label::new(|d: &ImageViewer, _: &Env| {
                format!("{:.02}", d.score_threshold)
            })),
            0.0,
        )
        .with_flex_child(Align::right(Label::new("min:")), 0.0)
        .with_flex_child(
            Align::right(
                TextBox::new()
                    .with_formatter(ParseFormatter::new())
                    .fix_width(40.)
                    .lens(ImageSliceConfig::zoom_min)
                    .lens(ImageViewer::image_slice_config),
            ),
            0.,
        )
        .with_flex_child(Align::right(Label::new("max:")), 0.0)
        .with_flex_child(
            Align::right(
                TextBox::new()
                    .with_formatter(ParseFormatter::new())
                    .fix_width(40.)
                    .lens(ImageSliceConfig::zoom_max)
                    .lens(ImageViewer::image_slice_config),
            ),
            0.,
        )
        .with_flex_child(Align::right(Label::new("#zooms:")), 0.0)
        .with_flex_child(
            Align::right(
                TextBox::new()
                    .with_formatter(ParseFormatter::new())
                    .fix_width(20.)
                    .lens(ImageSliceConfig::zoom_levels)
                    .lens(ImageViewer::image_slice_config),
            ),
            0.,
        )
        .with_flex_child(Align::right(increase_crop_level), 0.0)
        .with_flex_child(Align::right(decrease_crop_level), 0.0);
    // ######
    // All together
    // ######
    let mut layout = Flex::column();
    layout.add_child(button_bar);
    layout.add_child(img);
    layout
}

#[allow(unused_assignments)]
fn make_menu<T: Data>() -> MenuDesc<T> {
    let mut base = MenuDesc::empty();

    let tsv = FileSpec::new("TSV file", &["tsv", "TSV", "diam", "csv"]);
    let open_dialog_options = FileDialogOptions::new()
        .allowed_types(vec![tsv])
        .default_type(tsv)
        .default_name("")
        .name_label("Source")
        .title("Choose a tsv file with crater information")
        .button_text("Import");

    let toml = FileSpec::new("Config file", &["toml"]);
    let default_save_name = String::from("celeste_config.toml");
    let save_dialog_options = FileDialogOptions::new()
        .allowed_types(vec![toml])
        .default_type(toml)
        .default_name(default_save_name)
        .name_label("Target")
        .title("Choose a target for this lovely file")
        .button_text("Export");
    // FILE menu
    base = base.append(
        MenuDesc::new(LocalizedString::new("common-menu-file-menu"))
            .append(MenuItem::new(
                LocalizedString::new("celeste-menu-opencrater")
                    .with_placeholder("Open Crater file"),
                druid::commands::SHOW_OPEN_PANEL.with(open_dialog_options),
            ))
            .append(MenuItem::new(
                LocalizedString::new("celeste-menu-clear-data")
                    .with_placeholder("Clear Crater data"),
                MENU_CLEAR_CRATER.with(0),
            ))
            .append(MenuItem::new(
                LocalizedString::new("celeste-menu-export-crater")
                    .with_placeholder("Export Crater Data"),
                MENU_EXPORT_CRATER.with(0),
            ))
            .append(MenuItem::new(
                LocalizedString::new("celeste-menu-export-config")
                    .with_placeholder("Export Config"),
                SHOW_SAVE_PANEL.with(save_dialog_options),
            )),
    );
    // SELECT menu
    base = base.append(
        MenuDesc::new(LocalizedString::new("celeste-selection-options").with_placeholder("Select"))
            .append(MenuItem::new(
                LocalizedString::new("celeste-menu-select-all")
                    .with_placeholder("Select all craters"),
                MENU_SELECT_ALL_CRATERS.with(0),
            ))
            .append(MenuItem::new(
                LocalizedString::new("celeste-menu-deselect-all")
                    .with_placeholder("Deselect all craters"),
                MENU_DESELECT_ALL_CRATERS.with(0),
            ))
            .append(MenuItem::new(
                LocalizedString::new("celeste-menu-select-all-export")
                    .with_placeholder("Select all images for export"),
                MENU_SELECT_ALL_EXPORT.with(0),
            ))
            .append(MenuItem::new(
                LocalizedString::new("celeste-menu-deselect-all-export")
                    .with_placeholder("Deselect all images for export"),
                MENU_DESELECT_ALL_EXPORT.with(0),
            )),
    );
    // VIEW menu
    base = base.append(
        MenuDesc::new(LocalizedString::new("celeste-view-options").with_placeholder("View"))
            .append(MenuItem::new(
                LocalizedString::new("celeste-menu-toggle_crops")
                    .with_placeholder("Toggle image Crops"),
                MENU_TOGGLE_CROP_VIEW.with(0),
            ))
            .append(MenuItem::new(
                LocalizedString::new("celeste-menu-toggle-secondary")
                    .with_placeholder("Toggle Primaries/All crater"),
                MENU_TOGGLE_SECONDARY_SHOW.with(0),
            )),
    );
    // EXPORT menu
    base = base.append(
        MenuDesc::new(
            LocalizedString::new("celeste-export-options").with_placeholder("Image Export"),
        )
        .append(MenuItem::new(
            LocalizedString::new("export-image-current-training")
                .with_placeholder("Current zoom level for training"),
            MENU_IMAGE_EXPORT_CURRENT_TRAINING.with(0),
        ))
        .append(MenuItem::new(
            LocalizedString::new("export-image-all-training")
                .with_placeholder("All zooms levels for training"),
            MENU_IMAGE_EXPORT_ALL_TRAINING.with(0),
        ))
        .append(MenuItem::new(
            LocalizedString::new("export-image-current").with_placeholder("Current zoom"),
            MENU_IMAGE_EXPORT_CURRENT.with(0),
        ))
        .append(MenuItem::new(
            LocalizedString::new("export-image-all").with_placeholder("All zoom levels"),
            MENU_IMAGE_EXPORT_ALL.with(0),
        )),
    );
    base
}

impl AppDelegate<ImageViewer> for Delegate {
    fn command(
        &mut self,
        ctx: &mut DelegateCtx,
        _target: Target,
        cmd: &Command,
        data: &mut ImageViewer,
        _env: &Env,
    ) -> Handled {
        if let Some(file_info) = cmd.get(commands::OPEN_FILE) {
            let path = file_info.path();

            match path.extension() {
                Some(extension) => {
                    if extension == "tiff"
                        || extension == "tif"
                        || extension == "TIF"
                        || extension == "TIFF"
                    {
                        match path.to_str() {
                            Some(value) => {
                                println!("Loading TIFF image: {:?}", value);
                                data.image_path = Some(Arc::new(value.to_owned()));
                                data.reset();
                            }
                            None => {
                                eprintln!("Error reading file");
                            }
                        }
                    } else if extension == "tsv" {
                        match path.to_str() {
                            Some(value) => match craters::read_tsv_crater_data(value) {
                                Ok(craters) => {
                                    println!("Loading tsv crater file: {:?}", value);
                                    data.crater_data = Some(craters)
                                }
                                Err(_) => (),
                            },
                            None => {
                                eprintln!("Could not read tsv");
                            }
                        }
                    } else if extension == "diam" {
                        match path.to_str() {
                            Some(value) => match craters::read_diam_crater_data(value) {
                                Ok(craters) => {
                                    println!("Loading diam crater file: {:?}", value);
                                    data.crater_data = Some(craters)
                                }
                                Err(_) => (),
                            },
                            None => {
                                eprintln!("Could not read diam");
                            }
                        }
                    } else if extension == "csv" {
                        match path.to_str() {
                            Some(value) => match craters::read_csv_predicted_crater_data(value) {
                                Ok(craters) => {
                                    println!("Loading PREDICTIONS crater file: {:?}", value);
                                    data.crater_data = Some(craters)
                                }
                                Err(_) => (),
                            },
                            None => {
                                eprintln!("Could not read csv");
                            }
                        }
                    }
                }
                None => {
                    eprintln!("Error getting file extension");
                }
            }

            return Handled::Yes;
        } else if let Some(file_info) = cmd.get(commands::SAVE_FILE_AS) {
            let config = data.image_slice_config.export();
            let config_str = tools::serialize_config(&config);
            if let Err(e) = std::fs::write(file_info.path(), &config_str[..]) {
                eprintln!("Error writing file: {}", e);
            }
            return Handled::Yes;
        } else if cmd.is(MENU_CLEAR_CRATER) {
            data.clear_crater_data();
        } else if cmd.is(MENU_EXPORT_CRATER) {
            data.export_crater_data();
        } else if cmd.is(MENU_SELECT_ALL_CRATERS) {
            data.select_all_craters();
            data.viewer_mode = ViewerMode::Navigation;
        } else if cmd.is(MENU_DESELECT_ALL_CRATERS) {
            data.deselect_all_craters();
            data.viewer_mode = ViewerMode::Navigation;
        } else if cmd.is(MENU_TOGGLE_CROP_VIEW) {
            data.image_slice_config.hidden = !data.image_slice_config.hidden;
            ctx.submit_command(CURRENT_ZOOM_LEVEL_CHANGED.with(0));
        } else if cmd.is(MENU_TOGGLE_SECONDARY_SHOW) {
            data.show_only_primary = !data.show_only_primary;
        } else if cmd.is(MENU_SELECT_ALL_EXPORT) {
            if !data.image_slice_config.hidden {
                data.select_all_export_images();
            }
        } else if cmd.is(MENU_DESELECT_ALL_EXPORT) {
            if !data.image_slice_config.hidden {
                data.deselect_all_export_images();
            }
        }

        Handled::No
    }
}
