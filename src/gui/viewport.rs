use druid::keyboard_types::Key;
use druid::widget::FillStrat;
use druid::Code;
use geo::transformation::GeoTranslation;
// use druid::im::{vector, Vector};
use crate::image::ImageData;
use druid::{
    im::Vector,
    kurbo::{Circle, Line},
    piet::{InterpolationMode, PietImage},
    Affine, ArcStr, BoxConstraints, Color, Cursor, Data, Env, Event, EventCtx, FontDescriptor,
    FontFamily, LayoutCtx, Lens, LifeCycle, LifeCycleCtx, PaintCtx, Point, Rect, RenderContext,
    Size, TextLayout, UpdateCtx, Widget,
};
use std::path::PathBuf;
use std::sync::Arc;

use crate::processing::CropLines;
use crate::{
    labels::craters::{self, CraterData},
    processing,
    utils::tools::Config,
};

#[derive(Clone, Data, Default, Lens)]
pub struct ImageSliceConfig {
    pub output_raster: [u32; 2],
    pub zoom_levels: u8,
    pub zoom_min: f64,
    pub zoom_max: f64,
    pub current_level: u8,
    pub hidden: bool,
}

impl ImageSliceConfig {
    pub fn with_config(config: Config) -> Self {
        ImageSliceConfig {
            output_raster: config.output_dimensions,
            zoom_levels: config.zoom_levels,
            zoom_min: config.zoom_fraction_min,
            zoom_max: config.zoom_fraction_max,
            current_level: config.zoom_levels - 1,
            hidden: true,
        }
    }

    pub fn export(&self) -> Config {
        Config::new(
            self.output_raster,
            self.zoom_levels,
            self.zoom_min,
            self.zoom_max,
        )
    }
}

#[derive(Clone, Data, PartialEq)]
pub enum ViewerMode {
    Navigation,
    CraterEdit,
    CraterSelection,
    ExportSelection,
}

#[derive(Clone, Data, Lens)]
pub struct ImageViewer {
    pub image_path: Option<Arc<String>>,
    pub zoom_level: f64,
    pub origin: Point,
    pub is_dragging: bool,
    pub last_mouse_pos: Option<Point>,
    pub last_origin: Point,
    pub current_cursor_pos: Option<Point>,
    pub viewer_mode: ViewerMode,
    pub crater_data: Option<Vector<CraterData>>,
    pub score_threshold: f64,
    pub show_only_primary: bool,
    pub last_modified_crater: Option<(String, Point)>,
    pub image_slice_config: ImageSliceConfig,
    pub export_crop_lines: Option<CropLines>,
    pub export_images: Option<Vector<bool>>,
}

impl Default for ImageViewer {
    fn default() -> Self {
        Self::new()
    }
}

impl ImageViewer {
    pub fn new() -> Self {
        ImageViewer {
            image_path: None,
            zoom_level: 1.,
            origin: Point::ZERO,
            is_dragging: false,
            last_mouse_pos: None,
            last_origin: Point::ZERO,
            current_cursor_pos: None,
            viewer_mode: ViewerMode::Navigation,
            crater_data: None,
            score_threshold: 0.5,
            show_only_primary: false,
            last_modified_crater: None,
            image_slice_config: ImageSliceConfig::default(),
            export_crop_lines: None,
            export_images: None,
        }
    }

    pub fn with_config(config: Config) -> Self {
        let mut new_config = Self::new();
        new_config.image_slice_config = ImageSliceConfig::with_config(config);
        new_config
    }

    pub fn reset(&mut self) {
        self.zoom_level = 1.;
        self.origin = Point::ZERO;
        self.is_dragging = false;
        self.last_mouse_pos = None;
        self.last_origin = Point::ZERO;
        self.viewer_mode = ViewerMode::Navigation;
    }

    pub fn clear_crater_data(&mut self) {
        self.crater_data = None;
    }

    fn set_all_crater_selection(&mut self, is_selected: bool) {
        if let Some(craters) = self.crater_data.as_mut() {
            for c in craters.iter_mut() {
                c.selected = is_selected;
            }
        }
    }
    pub fn select_all_craters(&mut self) {
        self.set_all_crater_selection(true);
    }
    pub fn deselect_all_craters(&mut self) {
        self.set_all_crater_selection(false);
    }

    fn set_all_export_selection(&mut self, is_selected: bool) {
        if let Some(export_images) = self.export_images.as_mut() {
            for image in export_images.iter_mut() {
                *image = is_selected;
            }
        }
    }

    pub fn select_all_export_images(&mut self) {
        self.set_all_export_selection(true);
    }

    pub fn deselect_all_export_images(&mut self) {
        self.set_all_export_selection(false);
    }

    pub fn export_crater_data(&self) {
        match &self.crater_data {
            Some(craters) => {
                let _ = craters::export_modified_data(&craters);
            }
            None => (),
        }
    }
}

pub struct Image {
    image_data: ImageData,
    image_data_reduced: Option<ImageData>,
    reduced_zoom: f64,
    paint_data: Option<PietImage>,
    paint_data_reduced: Option<PietImage>,
    fill: FillStrat,
    interpolation: InterpolationMode,
}

impl Image {
    pub fn new(image_data: ImageData) -> Self {
        Image {
            image_data,
            image_data_reduced: None,
            reduced_zoom: 0.3,
            paint_data: None,
            paint_data_reduced: None,
            fill: FillStrat::ScaleDown,
            interpolation: InterpolationMode::Bilinear,
        }
    }
}

impl Widget<ImageViewer> for Image {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut ImageViewer, _env: &Env) {
        // Capture keyboard inputs
        ctx.request_focus();
        match event {
            Event::MouseDown(ev) => {
                data.last_mouse_pos = Some(ev.pos);
                data.is_dragging = true;
                if data.viewer_mode == ViewerMode::Navigation {
                    data.last_origin = data.origin;
                    ctx.set_cursor(&Cursor::OpenHand);
                }
            }
            Event::MouseUp(ev) => {
                data.is_dragging = false;
                ctx.set_cursor(&Cursor::Arrow);

                // Select craters
                if data.viewer_mode == ViewerMode::CraterSelection {
                    let mouse_deg = self.image_data.get_pos_as_deg(&data, &ev.pos);
                    if let Some(craters) = data.crater_data.as_mut() {
                        let mut craters_under_cursor = craters.iter_mut().filter(|c| {
                            let radius = self.image_data.model.meter_to_degrees(c.diameter_m) / 2.;
                            let distance_to_centeroid = c.coordinate.distance(&mouse_deg);
                            distance_to_centeroid <= radius
                        });
                        if let Some(crater) = craters_under_cursor.next() {
                            crater.selected = !crater.selected;
                        }
                    }
                } else if data.viewer_mode == ViewerMode::ExportSelection {
                    if let Some(crop_lines) = data.export_crop_lines.as_ref() {
                        let mut idx: [usize; 2] = [0; 2];
                        let mut found_h = false;
                        let mut found_v = false;

                        let num_h_squares = crop_lines.horizontal.len() - 1;
                        let num_v_squares = crop_lines.vertical.len() - 1;

                        for h_idx in 0..num_h_squares {
                            let upper = crop_lines.horizontal.get(h_idx);
                            let lower = crop_lines.horizontal.get(h_idx + 1);

                            if let (Some(upper), Some(lower)) = (upper, lower) {
                                if ev.pos.y >= data.origin.y + *upper as f64 * data.zoom_level
                                    && ev.pos.y < data.origin.y + *lower as f64 * data.zoom_level
                                {
                                    idx[1] = h_idx;
                                    found_h = true;
                                    break;
                                }
                            }
                        }

                        for v_idx in 0..num_v_squares {
                            let upper = crop_lines.vertical.get(v_idx);
                            let lower = crop_lines.vertical.get(v_idx + 1);

                            if let (Some(upper), Some(lower)) = (upper, lower) {
                                if ev.pos.x >= data.origin.x + *upper as f64 * data.zoom_level
                                    && ev.pos.x < data.origin.x + *lower as f64 * data.zoom_level
                                {
                                    idx[0] = v_idx;
                                    found_v = true;
                                    break;
                                }
                            }
                        }

                        let bitmap_length = (num_h_squares) * (num_v_squares);

                        if found_v
                            && found_h
                            && data.export_images.is_some()
                            && data.export_images.as_ref().unwrap().len() >= bitmap_length
                        {
                            let i = idx[1] * num_v_squares + idx[0];

                            let old_value = *data.export_images.as_ref().unwrap().get(i).unwrap();
                            data.export_images.as_mut().unwrap().set(i, !old_value);
                        }
                    }
                }

                // Edit craters
                if let Some(last_c) = data.last_modified_crater.as_mut() {
                    // Modify the crater with the id and convert mouse position
                    if let Some(craters) = data.crater_data.as_mut() {
                        let mut dragged_crater =
                            craters.iter_mut().filter(|c| c.crater_id.eq(&last_c.0));
                        if let Some(value) = dragged_crater.next() {
                            let difference = ev.pos - last_c.1;
                            let center_raster = self
                                .image_data
                                .model
                                .to_raster_from_latlong(value.coordinate);

                            let center_raster_corrected = (
                                center_raster[0] + difference.x / data.zoom_level,
                                center_raster[1] + difference.y / data.zoom_level,
                            );
                            let corrected_deg = self.image_data.model.to_latlong_deg(
                                center_raster_corrected.0,
                                center_raster_corrected.1,
                            );
                            value.coordinate = corrected_deg;
                            value.updated = true;
                        }
                    }
                }
                data.last_modified_crater = None;
            }
            Event::MouseMove(ev) => {
                data.current_cursor_pos = Some(ev.pos);
                if data.is_dragging {
                    if let Some(last_mouse_pos) = data.last_mouse_pos {
                        let difference = ev.pos - last_mouse_pos;
                        if data.viewer_mode == ViewerMode::CraterEdit {
                            let mouse_deg = self.image_data.get_pos_as_deg(&data, &ev.pos);
                            if let Some(craters) = data.crater_data.as_mut() {
                                let mut craters_under_cursor = craters.iter_mut().filter(|c| {
                                    let radius =
                                        self.image_data.model.meter_to_degrees(c.diameter_m) / 2.;
                                    let distance_to_centeroid = c.coordinate.distance(&mouse_deg);
                                    distance_to_centeroid <= radius
                                });
                                if data.last_modified_crater.is_none() {
                                    if let Some(value) = craters_under_cursor.next() {
                                        // Go through craters which are smaller than radius
                                        // away from cursor and then chose the first one and if
                                        // it was not set before (while dragging), save it's ID
                                        // and mouse coordinates.
                                        let new_crater = Some((value.crater_id.clone(), ev.pos));
                                        data.last_modified_crater = new_crater;
                                    }
                                }
                            }
                        } else if data.viewer_mode == ViewerMode::Navigation {
                            data.origin = data.last_origin + difference;
                        }
                    }
                }
                ctx.request_paint();
            }
            Event::Wheel(ev) => {
                let old_zoom_level = data.zoom_level;
                let max_zoom = 2.;
                let scaler = if ev.wheel_delta.y > 0. { 1. / 1.1 } else { 1.1 };
                let zoomed = old_zoom_level * scaler;

                let new_zoom_level = if zoomed < max_zoom { zoomed } else { max_zoom };

                if new_zoom_level <= max_zoom {
                    let from_img_to_window = |p: Point, scale: f64| -> Point {
                        Point::new(data.origin.x + (p.x * scale), data.origin.y + (p.y * scale))
                    };
                    let from_window_to_img = |p: Point, scale: f64| -> Point {
                        Point::new((p.x - data.origin.x) / scale, (p.y - data.origin.y) / scale)
                    };

                    let old_mouse_img = from_window_to_img(ev.pos, old_zoom_level);
                    let new_mouse_win = from_img_to_window(old_mouse_img, new_zoom_level);

                    let diff = ev.pos - new_mouse_win;
                    data.origin += diff;
                }
                data.last_mouse_pos = Some(ev.pos);
                data.zoom_level = new_zoom_level;
            }
            Event::KeyDown(k) => {
                if k.key == Key::Escape {
                    data.viewer_mode = ViewerMode::Navigation;
                } else if k.code == Code::KeyX {
                    if data.image_path.is_some() && !data.image_slice_config.hidden {
                        data.viewer_mode = if data.viewer_mode != ViewerMode::ExportSelection {
                            ViewerMode::ExportSelection
                        } else {
                            ViewerMode::Navigation
                        };
                    }
                } else if k.code == Code::KeyE {
                    if data.image_path.is_some() && data.crater_data.is_some() {
                        data.viewer_mode = if data.viewer_mode != ViewerMode::CraterEdit {
                            ViewerMode::CraterEdit
                        } else {
                            ViewerMode::Navigation
                        };
                    }
                } else if k.code == Code::KeyS {
                    if data.image_path.is_some() && data.crater_data.is_some() {
                        data.viewer_mode = if data.viewer_mode != ViewerMode::CraterSelection {
                            ViewerMode::CraterSelection
                        } else {
                            ViewerMode::Navigation
                        };
                    }
                } else if k.code == Code::Minus {
                    // zoom out
                    //
                    if data.image_slice_config.current_level
                        < data.image_slice_config.zoom_levels - 1
                    {
                        data.image_slice_config.current_level += 1;
                        ctx.submit_command(super::admin_gui::CURRENT_ZOOM_LEVEL_CHANGED.with(0));
                    }
                //ctx.submit_command(crate::gui::admin_gui::CURRENT_ZOOM_LEVEL_CHANGED.with(0));
                } else if k.code == Code::Equal {
                    // zoom in
                    if data.image_slice_config.current_level > 0 {
                        data.image_slice_config.current_level -= 1;
                        ctx.submit_command(super::admin_gui::CURRENT_ZOOM_LEVEL_CHANGED.with(0));
                    }
                }
            }
            Event::Command(cmd) => {
                if cmd.is(crate::gui::admin_gui::MENU_IMAGE_EXPORT_CURRENT_TRAINING) {
                    if data.crater_data.is_none() {
                        eprintln!(
                            "no crater data selected, cannot export images for training. \
                                  crater data is needed for training"
                        );
                        return;
                    }
                    let _ = crate::processing::crop_image(
                        &self.image_data,
                        &data.image_slice_config.export(),
                        Some(data.image_slice_config.current_level),
                        true,
                        &data.crater_data,
                        &data.export_images,
                        None,
                    );
                } else if cmd.is(crate::gui::admin_gui::MENU_IMAGE_EXPORT_CURRENT) {
                    let _ = crate::processing::crop_image(
                        &self.image_data,
                        &data.image_slice_config.export(),
                        Some(data.image_slice_config.current_level),
                        false,
                        &None,
                        &None,
                        None,
                    );
                } else if cmd.is(crate::gui::admin_gui::MENU_IMAGE_EXPORT_ALL_TRAINING) {
                    if data.crater_data.is_none() {
                        eprintln!(
                            "no crater data selected, cannot export images for training. \
                                  crater data is needed for training"
                        );
                        return;
                    }
                    let _ = crate::processing::crop_image(
                        &self.image_data,
                        &data.image_slice_config.export(),
                        None,
                        true,
                        &data.crater_data,
                        &data.export_images,
                        None,
                    );
                } else if cmd.is(crate::gui::admin_gui::MENU_IMAGE_EXPORT_ALL) {
                    let _ = crate::processing::crop_image(
                        &self.image_data,
                        &data.image_slice_config.export(),
                        None,
                        false,
                        &None,
                        &None,
                        None,
                    );
                } else if cmd.is(crate::gui::admin_gui::CURRENT_ZOOM_LEVEL_CHANGED) {
                    if data.image_slice_config.current_level >= data.image_slice_config.zoom_levels
                    {
                        data.image_slice_config.current_level = data.image_slice_config.zoom_levels;
                    }
                    let image_size = self.image_data.get_size().to_rect();
                    data.export_crop_lines = Some(processing::get_crops(
                        &data.image_slice_config.export(),
                        data.image_slice_config.current_level,
                        [image_size.width(), image_size.height()],
                    ));
                    // data.export_images = Some(Vector::)
                    if let Some(ref crop_lines) = data.export_crop_lines {
                        let length =
                            (crop_lines.horizontal.len() - 1) * (crop_lines.vertical.len() - 1);
                        // This is probably inefficient:
                        let mut bitmap = Vector::new();
                        for _ in 0..length {
                            bitmap.push_back(false);
                        }
                        data.export_images = Some(bitmap);
                    }
                }
            }
            _ => {}
        }
    }

    fn lifecycle(
        &mut self,
        _ctx: &mut LifeCycleCtx,
        _event: &LifeCycle,
        _data: &ImageViewer,
        _env: &Env,
    ) {
    }

    fn update(
        &mut self,
        ctx: &mut UpdateCtx,
        old_data: &ImageViewer,
        data: &ImageViewer,
        _env: &Env,
    ) {
        if !old_data.image_path.same(&data.image_path) {
            let path = match data.image_path {
                Some(ref value) => PathBuf::from(value.as_ref()),
                None => return,
            };

            match ImageData::from_data_tiff(path) {
                Ok(image_data) => {
                    self.image_data = image_data;
                    self.image_data_reduced = self.image_data.resize(self.reduced_zoom).ok();

                    self.paint_data = None;
                    self.paint_data_reduced = None;
                    ctx.request_layout();

                    let upper_left_latlong = self.image_data.model.to_latlong_deg(0., 0.);
                    let upper_left_meter = self.image_data.model.to_xy_meter(0., 0.);
                    println!(
                        "Upper left for raster: (0,0), deg: (long: {:?}, lat: {:?}), meter: ({:?}, {:?})",
                        upper_left_latlong.long,
                        upper_left_latlong.lat,
                        upper_left_meter[0],
                        upper_left_meter[1]
                    );
                }
                Err(err) => {
                    eprintln!("{}", err)
                }
            }
        }
        if !old_data.zoom_level.same(&data.zoom_level)
            || !old_data.crater_data.same(&data.crater_data)
            || !old_data.export_images.same(&data.export_images)
            || !old_data.show_only_primary.same(&data.show_only_primary)
        {
            ctx.request_paint();
        }
        if !old_data.viewer_mode.same(&data.viewer_mode) {
            if data.viewer_mode != ViewerMode::Navigation {
                ctx.set_cursor(&Cursor::Arrow);
            }
        }
        if !old_data.image_slice_config.same(&data.image_slice_config) {
            if !old_data
                .image_slice_config
                .zoom_levels
                .same(&data.image_slice_config.zoom_levels)
                || !old_data
                    .image_slice_config
                    .zoom_max
                    .same(&data.image_slice_config.zoom_max)
                || !old_data
                    .image_slice_config
                    .zoom_min
                    .same(&data.image_slice_config.zoom_min)
            {
                ctx.submit_command(crate::gui::admin_gui::CURRENT_ZOOM_LEVEL_CHANGED.with(0));
            }
            ctx.request_paint()
        }
    }

    fn layout(
        &mut self,
        _layout_ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        _data: &ImageViewer,
        _env: &Env,
    ) -> Size {
        bc.debug_check("Image");
        bc.constrain(self.image_data.get_size())
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &ImageViewer, env: &Env) {
        // //////////
        // Calculate origin and mouse position
        // //////////

        // The ImageData's to_piet function does not clip to the image's size
        // CairoRenderContext is very like druids but with some extra goodies like clip
        if self.fill != FillStrat::Contain {
            let clip_rect = Rect::ZERO.with_size(ctx.size());
            ctx.clip(clip_rect);
        }

        let scale = data.zoom_level;
        // origin is the top-left most point wrt to the window top-left point
        let offset_matrix = Affine::new([scale, 0., 0., scale, data.origin.x, data.origin.y]);

        ctx.with_save(|ctx| {
            if scale > self.reduced_zoom || self.image_data_reduced.is_none() {
                let image_data = &self.image_data;
                let piet_image = self
                    .paint_data
                    .get_or_insert_with(|| to_piet(&image_data, ctx));
                ctx.transform(offset_matrix);
                ctx.draw_image(
                    piet_image,
                    self.image_data.get_size().to_rect(),
                    self.interpolation,
                );
            } else {
                let image_data = self.image_data_reduced.as_ref().unwrap();
                let piet_image = self
                    .paint_data_reduced
                    .get_or_insert_with(|| to_piet(&image_data, ctx));
                ctx.transform(offset_matrix);
                ctx.draw_image(
                    piet_image,
                    self.image_data.get_size().to_rect(),
                    self.interpolation,
                );
            }
        });

        // //////////
        // setup text overlay box
        // //////////

        // //////////
        // Mode info text

        let mode_text = if data.viewer_mode == ViewerMode::CraterSelection {
            "SELECT"
        } else if data.viewer_mode == ViewerMode::CraterEdit {
            "EDIT"
        } else if data.viewer_mode == ViewerMode::ExportSelection {
            "EXPORT"
        } else {
            "NAV"
        };

        // //////////
        // Coordinate text

        let text = match data.current_cursor_pos {
            Some(value) => {
                let (image_x, image_y) = (
                    (value.x - data.origin.x) / scale,
                    (value.y - data.origin.y) / scale,
                );
                let latlong_deg = self.image_data.model.to_latlong_deg(image_x, image_y);
                let coordinates_m = self.image_data.model.to_xy_meter(image_x, image_y);
                let all_or_secondary = if data.show_only_primary { "PRM" } else { "ALL" };
                format!(
                    "{:>3.0}% ({:.2}m, {:.2}m), ({:.4}°E, {:.4}°N) | {} | {} | {}",
                    scale * 100.,
                    coordinates_m[0],
                    coordinates_m[1],
                    latlong_deg.long,
                    latlong_deg.lat,
                    self.image_data.name,
                    all_or_secondary,
                    mode_text,
                )
            }
            None => String::from(mode_text),
        };

        let mut label = TextLayout::<ArcStr>::from_text(text);
        label.set_font(FontDescriptor::new(FontFamily::SANS_SERIF).with_size(12.0));
        label.set_text_color(Color::rgba8(0xFF, 0xFF, 0xFF, 0xFF));
        label.rebuild_if_needed(ctx.text(), env);

        let size = ctx.region().rects()[0];
        let rect = Rect::from_origin_size(
            (0., size.y1 - label.size().height - (2. * 5.)),
            (label.size().width + 15., label.size().height + 10.),
        );

        let fill_color = Color::rgba8(0x00, 0x00, 0x00, 0x7F);
        ctx.fill(rect, &fill_color);

        let red_color = Color::rgba8(0xFF, 0x00, 0x00, 0x7F);
        let blue_color = Color::rgba8(0x00, 0x00, 0xFF, 0x7F);
        let green_color = Color::rgba8(0x00, 0x9F, 0x00, 0x6F);
        let secondary_color = Color::rgba8(0x2A, 0xB6, 0x0A, 0x7F);

        // Adding the text label with info on coordinates and zoom level
        ctx.with_save(|ctx| {
            label.draw(ctx, (5., size.y1 - label.size().height - 5.));
        });

        // //////////
        // Draw craters
        // //////////
        if let Some(craters) = &data.crater_data {
            let start_coord = self.image_data.model.to_latlong_deg(0., 0.);
            let end_coord = self.image_data.model.to_latlong_deg(
                self.image_data.get_size().width,
                self.image_data.get_size().height,
            );

            let image_craters = craters.iter().filter(|c| {
                // If craters contain a prediction score, only show the ones with an
                // equal or higher score than the current threshold
                let score_or_true = match c.prediction_score {
                    Some(score) => score as f64 >= data.score_threshold,
                    None => true,
                };

                // If show_only_primary is set, all craters which are not secondary should be
                // selected, otherwise it does not matter what secondary is for each crater
                // Crater data that contains secondary information is distinct from crater data
                // that contains prediction scores.
                let primary_or_true = if data.show_only_primary {
                    !c.secondary
                } else {
                    true
                };

                c.coordinate.long >= start_coord.long
                        && c.coordinate.long <= end_coord.long
                        && c.coordinate.lat <= start_coord.lat // lat increases upwards, raster_x downwards
                        && c.coordinate.lat >= end_coord.lat
                        && primary_or_true
                        && score_or_true
            });

            for c in image_craters {
                let p = self.image_data.model.to_raster_from_latlong(c.coordinate);

                let mut p_point =
                    Point::new(p[0] * scale + data.origin.x, p[1] * scale + data.origin.y);
                let radius_pixel =
                    (c.diameter_m * scale) / (self.image_data.model.pixel_size[0] * 2.);
                let circ = Circle::new(p_point, radius_pixel);

                if let (Some(last_crater), Some(cursor)) =
                    (&data.last_modified_crater, data.current_cursor_pos)
                {
                    if c.crater_id.eq(&last_crater.0) {
                        if let Some(last_pos) = data.last_mouse_pos {
                            let grey_color = Color::rgba8(0x55, 0x55, 0x55, 0x55);

                            let difference = cursor - last_pos;
                            p_point += difference;

                            let circ2 = Circle::new(p_point, radius_pixel);
                            ctx.stroke(circ2, &grey_color, 2.);
                        }
                    }
                }
                let centroid = Circle::new(p_point, 2. * scale);
                if c.updated {
                    ctx.stroke(circ, &blue_color, 2.);
                } else if !c.secondary {
                    ctx.stroke(circ, &red_color, 2.);
                } else {
                    ctx.stroke(circ, &secondary_color, 2.);
                }
                if c.selected {
                    ctx.fill(circ, &green_color);
                }
                ctx.fill(centroid, &red_color);
            }
        }

        // //////////
        // Draw slices
        // //////////
        if !data.image_slice_config.hidden && data.image_slice_config.zoom_levels > 0 {
            let yellow_color = Color::rgba8(0xFF, 0xFF, 0x00, 0x7F);
            let pale_yellow_color = Color::rgba8(0xFF, 0xFF, 0x00, 0x2F);
            // Visual indicator of current zoom level
            let label_text = format!(
                "{}/{}",
                data.image_slice_config.zoom_levels - data.image_slice_config.current_level,
                data.image_slice_config.zoom_levels
            );
            let mut label = TextLayout::<ArcStr>::from_text(label_text);
            label.set_font(FontDescriptor::new(FontFamily::SANS_SERIF).with_size(12.0));
            label.set_text_color(yellow_color.clone());
            label.rebuild_if_needed(ctx.text(), env);
            let size = ctx.region().rects()[0];
            let rect = Rect::from_origin_size(
                (size.x1 - label.size().width - (2. * 5.), 0.),
                (label.size().width + 15., label.size().height + 10.),
            );
            let fill_color = Color::rgba8(0x00, 0x00, 0x00, 0x7F);
            ctx.fill(rect, &fill_color);
            ctx.with_save(|ctx| {
                label.draw(ctx, (size.x1 - label.size().width - 5., 5.));
            });
            // Calculate and draw cop grid

            if let Some(crop_lines) = data.export_crop_lines.as_ref() {
                let last_vertical = *crop_lines.vertical.last().unwrap_or(&0);
                let last_horizontal = *crop_lines.horizontal.last().unwrap_or(&0);
                for h in &crop_lines.horizontal {
                    let line = Line::new(
                        Point::new(data.origin.x, data.origin.y + (*h as f64 * scale)),
                        Point::new(
                            data.origin.x + last_vertical as f64 * scale,
                            data.origin.y + (*h as f64 * scale),
                        ),
                    );
                    ctx.stroke(line, &yellow_color, 2.);
                }

                for v in &crop_lines.vertical {
                    let line = Line::new(
                        Point::new(data.origin.x + (*v as f64 * scale), data.origin.y),
                        Point::new(
                            data.origin.x + (*v as f64 * scale),
                            data.origin.y + (last_horizontal as f64 * scale),
                        ),
                    );
                    ctx.stroke(line, &yellow_color, 2.);
                }

                let num_h_squares = crop_lines.horizontal.len() - 1;
                let num_v_squares = crop_lines.vertical.len() - 1;
                // Draw rectangles around selected export
                for (i, bit) in data.export_images.as_ref().unwrap().iter().enumerate() {
                    if *bit {
                        let y_idx = i / num_v_squares;
                        let x_idx = i % num_h_squares;
                        if let (Some(v), Some(h), Some(v_next), Some(h_next)) = (
                            crop_lines.vertical.get(x_idx),
                            crop_lines.horizontal.get(y_idx),
                            crop_lines.vertical.get(x_idx + 1),
                            crop_lines.horizontal.get(y_idx + 1),
                        ) {
                            let position = Point::new(
                                data.origin.x + (*v as f64 * scale),
                                data.origin.y + (*h as f64 * scale),
                            );
                            let size = Size::new(
                                (*v_next as f64 - *v as f64) * scale,
                                (*h_next as f64 - *h as f64) * scale,
                            );
                            let rect = Rect::from_origin_size(position, size);
                            ctx.fill(rect, &pale_yellow_color);
                        }
                    }
                }
            }
        }
    }
}

fn to_piet(img: &ImageData, ctx: &mut PaintCtx) -> PietImage {
    ctx.make_image(
        img.get_size().width as usize,
        img.get_size().height as usize,
        &img.pixels,
        img.format,
    )
    .unwrap()
}
