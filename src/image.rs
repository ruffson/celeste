use crate::gui::viewport::ImageViewer;
use core::fmt;
use druid::piet::ImageFormat;
use druid::{Point, Size};
use geo::geotiff::geokey::{GeoKey, GeoKeyType};
use geo::transformation::{EquiRectangular, GeoTranslation, LatLong};
use once_cell::sync::Lazy;
use resize::Pixel::Gray8;
use resize::Type::{Lanczos3, Mitchell};
use std::path::{Path, PathBuf};

use regex::Regex;
use std::{error::Error, io};
use tiff::decoder::Limits;

use rgb::FromSlice;
// TODO: remove piet/druid dependencies from here
// TODO: remove ImageViewer as well
/// Processed image data.
///
/// By default, Druid does not parse image data.
/// However, enabling [the `image` feature]
/// provides several
/// methods by which you can load image files.
///
/// Contains raw bytes, dimensions, and image format ([`piet::ImageFormat`]).
///
/// [the `image` feature]: ../index.html#optional-features
/// [`piet::ImageFormat`]: ../piet/enum.ImageFormat.html
#[derive(Clone)]
pub struct ImageData {
    pub name: String,
    pub pixels: Vec<u8>,
    pub x_pixels: u32,
    pub y_pixels: u32,
    pub format: ImageFormat,
    pub model: EquiRectangular,
}

impl ImageData {
    /// Create an empty Image
    pub fn empty() -> Self {
        ImageData {
            name: String::from("Unknown"),
            pixels: [].to_vec(),
            x_pixels: 0,
            y_pixels: 0,
            format: ImageFormat::Grayscale,
            model: EquiRectangular::empty(),
        }
    }

    /// Create new image from data, x and y dimensions
    pub fn new(
        raw_path: PathBuf,
        pixels: Vec<u8>,
        x_pixels: u32,
        y_pixels: u32,
        format: ImageFormat,
        model: EquiRectangular,
    ) -> Self {
        let name = extract_region_name(&raw_path).unwrap_or_else(|| "Unknown".into());
        ImageData {
            name,
            pixels,
            x_pixels,
            y_pixels,
            format,
            model,
        }
    }

    fn duplicate_with(
        &self,
        pixels: Vec<u8>,
        x_pixels: u32,
        y_pixels: u32,
        model: EquiRectangular,
    ) -> Self {
        ImageData {
            name: self.name.clone(),
            pixels,
            x_pixels,
            y_pixels,
            format: self.format,
            model,
        }
    }

    /// Get the size in pixels of the contained image.
    pub fn get_size(&self) -> Size {
        Size::new(self.x_pixels as f64, self.y_pixels as f64)
    }

    pub fn resize(&self, scaler: f64) -> Result<ImageData, Box<dyn std::error::Error>> {
        if scaler == 1. {
            return Ok(self.clone());
        }
        let (dst_x, dst_y) = (
            (self.x_pixels as f64 * scaler) as usize,
            (self.y_pixels as f64 * scaler) as usize,
        );
        let src = self.pixels.clone();
        let mut dst = vec![0; dst_x * dst_y];
        let mut resizer = if scaler < 1. {
            resize::new(
                self.x_pixels as usize,
                self.y_pixels as usize,
                dst_x,
                dst_y,
                Gray8,
                Lanczos3,
            )?
        } else {
            resize::new(
                self.x_pixels as usize,
                self.y_pixels as usize,
                dst_x,
                dst_y,
                Gray8,
                Mitchell,
            )?
        };
        let scaled_model = self.model.get_scaled(scaler);
        resizer.resize(src.as_gray(), dst.as_gray_mut())?;
        Ok(self.duplicate_with(dst, dst_x as u32, dst_y as u32, scaled_model))
    }
}

fn extract_region_name(path: &Path) -> Option<String> {
    let name = path.file_stem()?.to_str()?;
    // (1) We don't compile/construct the regex every time this function is called but merely once
    // (2) We can (and should!) unwrap the regex here as we *know* that we provided a legal regex!
    static PATTERN: Lazy<Regex> = Lazy::new(|| Regex::new(r"^[^_]+_[^_]+_[^_]+_(.+)+$").unwrap());
    let caps = PATTERN.captures(name)?;
    let this_is_what_i_want = caps.get(1)?.as_str();
    Some(this_is_what_i_want.into())
}

impl ImageData {
    // Helper functions
    pub fn get_pos_as_deg(&self, data: &ImageViewer, pos: &Point) -> LatLong {
        self.model.to_latlong_deg(
            (pos.x - data.origin.x) / data.zoom_level,
            (pos.y - data.origin.y) / data.zoom_level,
        )
    }

    pub fn from_data_tiff(raw_image_path: PathBuf) -> Result<Self, Box<dyn Error>> {
        let mut big_limits = Limits::default();
        big_limits.decoding_buffer_size = 1024 * 1024 * 1024;

        let dyn_img = std::fs::File::open(&std::path::Path::new(&raw_image_path))?;
        let mut decoder = tiff::decoder::Decoder::new(dyn_img)?.with_limits(big_limits);

        let decoder_result = decoder.read_image()?;

        match decoder_result {
            tiff::decoder::DecodingResult::U8(img_res) => {
                // let software = decoder.get_tag(tiff::tags::Tag::ImageWidth).unwrap();

                let geodir = decoder.get_tag_u16_vec(tiff::tags::Tag::GeoKeyDirectoryTag)?;
                let ascii_params = decoder
                    .get_tag_ascii_string(tiff::tags::Tag::GeoAsciiParamsTag)
                    .ok();
                let double_params = decoder
                    .get_tag_f64_vec(tiff::tags::Tag::GeoDoubleParamsTag)
                    .ok();
                let geokeydir = geo::get_geokeydir(geodir, ascii_params, double_params).unwrap();

                let radius = match geokeydir.get(&GeoKey::GeogSemiMinorAxisGeoKey).unwrap() {
                    GeoKeyType::Double(value) => value[0],
                    _ => {
                        return Err(Box::new(io::Error::new(
                            io::ErrorKind::InvalidData,
                            "Loaded an unsupported image.",
                        )))
                    }
                };
                let pixel_size = match decoder.get_tag(tiff::tags::Tag::ModelPixelScaleTag)? {
                    tiff::decoder::ifd::Value::List(vec) => {
                        let mut arr = [0.; 3];
                        let mut i = 0;
                        for v in vec {
                            arr[i] = v.into_f64()?;
                            i += 1;
                        }
                        arr
                    }
                    _ => {
                        return Err(Box::new(io::Error::new(
                            io::ErrorKind::InvalidData,
                            "Loaded an unsupported image.",
                        )))
                    }
                };
                let model_tiepoints = match decoder.get_tag(tiff::tags::Tag::ModelTiepointTag)? {
                    tiff::decoder::ifd::Value::List(vec) => {
                        let mut new_vec = Vec::with_capacity(vec.len());
                        for v in vec {
                            new_vec.push(v.into_f64()?);
                        }
                        new_vec
                    }
                    _ => {
                        return Err(Box::new(io::Error::new(
                            io::ErrorKind::InvalidData,
                            "Loaded an unsupported image.",
                        )))
                    }
                };

                let model = EquiRectangular::new(radius, pixel_size, model_tiepoints);

                let dimensions = decoder.dimensions().unwrap();

                if img_res.len() == dimensions.0 as usize * dimensions.1 as usize {
                    Ok(ImageData::new(
                        raw_image_path,
                        img_res,
                        dimensions.0,
                        dimensions.1,
                        ImageFormat::Grayscale,
                        model,
                    ))
                } else {
                    Ok(ImageData::new(
                        raw_image_path,
                        img_res,
                        dimensions.0,
                        dimensions.1,
                        ImageFormat::Rgb,
                        model,
                    ))
                }
            }
            _ => Err(Box::new(io::Error::new(
                io::ErrorKind::InvalidData,
                "Loaded an unsupported image.",
            ))),
        }
    }
}

impl Default for ImageData {
    fn default() -> Self {
        ImageData::empty()
    }
}

impl fmt::Debug for ImageData {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("ImageData")
            .field("size", &self.pixels.len())
            .field("width", &self.x_pixels)
            .field("height", &self.y_pixels)
            .field("format", &format_args!("{:?}", self.format))
            .finish()
    }
}
