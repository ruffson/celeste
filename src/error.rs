use std::{error::Error, fmt::Display};

#[derive(Debug)]
pub struct ConfigurationError {
    description: String,
}

impl ConfigurationError {
    pub fn with_description(description: String) -> Self {
        ConfigurationError { description }
    }
}

impl Display for ConfigurationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Configuration Error! \n{:?}", self.description)
    }
}

impl Error for ConfigurationError {}
