use crate::image::ImageData;
use crate::{error, labels::craters::CraterData, utils::tools::Config};
use druid::{im::Vector, Data};
use geo::transformation::{EquiRectangular, GeoTranslation};
use image::GrayImage;
use rayon::prelude::*;
use serde::Serialize;
use std::ffi::OsStr;
use std::time::Instant;
use std::{
    fs,
    io::Write,
    path::{Path, PathBuf},
};
use walkdir::{DirEntry, WalkDir};

const BASE_PATH: &str = "celeste_export";

#[derive(Clone, Data, Default, Debug)]
pub struct CropLines {
    pub horizontal: Vector<u32>,
    pub vertical: Vector<u32>,
}

impl CropLines {
    pub fn new(horizontal: Vector<u32>, vertical: Vector<u32>) -> Self {
        CropLines {
            horizontal,
            vertical,
        }
    }
}

#[derive(Debug, Serialize)]
pub struct CelesteImageExport {
    orig_name: String,
    model: EquiRectangular,
    scaler: f64,
    pub crater_data: Option<Vec<CraterData>>,
}

impl CelesteImageExport {
    pub fn new(
        orig_name: String,
        model: &EquiRectangular,
        scaler: f64,
        crater_data: Option<Vec<CraterData>>,
    ) -> Self {
        CelesteImageExport {
            orig_name,
            model: model.clone(),
            scaler,
            crater_data,
        }
    }
}

fn save_crops(
    path: &mut PathBuf,
    config: &Config,
    image: Vec<u8>,
    pixel_dimensions: [u32; 2],
    model: EquiRectangular,
    scaler: f64,
    file_name: &String,
    crater: &Option<Vector<CraterData>>,
    export_images: &Option<Vector<bool>>,
) -> Result<(), Box<dyn std::error::Error>> {
    let (num_rows, num_cols) = (
        (pixel_dimensions[0] as u32 / config.output_dimensions[0]) as usize,
        (pixel_dimensions[1] as u32 / config.output_dimensions[1]) as usize,
    );

    // Setup image buffers
    let mut sub_image_buffer: Vec<Vec<u8>> = Vec::with_capacity(num_cols);
    let mut counter = 0;
    for line in 0..pixel_dimensions[1] {
        for column in 0..num_cols {
            if line == 0 {
                // initialize vector
                sub_image_buffer.push(Vec::with_capacity(
                    (config.output_dimensions[0] * config.output_dimensions[1]) as usize,
                ));
            }

            let offset = line * pixel_dimensions[0] as u32;
            let start = (offset + (column as u32 * config.output_dimensions[0])) as usize;
            let stop = (offset + ((column as u32 + 1) * config.output_dimensions[0])) as usize;
            let slice = &image[start..stop];

            sub_image_buffer
                .get_mut(column)
                .unwrap()
                .extend_from_slice(slice);

            // Check if this image is complete
            if (line + 1) % config.output_dimensions[1] as u32 == 0 {
                counter += 1;
                if (counter - 1) >= num_cols * num_rows {
                    return Ok(());
                }

                // Reset old column buffer
                let column_vec = sub_image_buffer.get(column).unwrap().to_vec();
                sub_image_buffer.get_mut(column).unwrap().clear();
                // if the image is marked for export, skip it
                match export_images {
                    Some(bitmap) => {
                        let is_selected = bitmap.get(counter - 1).unwrap();
                        if !is_selected {
                            continue;
                        }
                    }
                    None => {}
                }

                // Crop the model
                let crop_model = model.get_offset(vec![
                    column as u32 * config.output_dimensions[0],
                    line - (config.output_dimensions[1] - 1),
                    0,
                ]);

                let mut export =
                    CelesteImageExport::new(file_name.clone(), &crop_model, scaler, None);

                if let Some(crater) = crater {
                    // TRAINING
                    let top_left = crop_model.to_latlong_deg(0., 0.);
                    let bottom_left =
                        crop_model.to_latlong_deg(0., config.output_dimensions[1] as f64);
                    let top_right =
                        crop_model.to_latlong_deg(config.output_dimensions[0] as f64, 0.);
                    let bottom_right = crop_model.to_latlong_deg(
                        config.output_dimensions[0] as f64,
                        config.output_dimensions[1] as f64,
                    );

                    // Select all craters which may "reach into" the crop
                    let mut contained_craters: Vec<CraterData> = crater
                        .iter()
                        .filter(|c| {
                            if !c.selected {
                                return false;
                            }

                            let r = crop_model.meter_to_degrees(c.diameter_m / 2.);
                            // Check if crater reaches inside from top/left/right/bottom
                            let is_right = c.coordinate.long - r <= bottom_right.long
                                && c.coordinate.long - r >= top_left.long
                                && c.coordinate.lat >= bottom_right.lat
                                && c.coordinate.lat <= top_left.lat;
                            let is_left = c.coordinate.long + r >= top_left.long
                                && c.coordinate.long + r <= bottom_right.long
                                && c.coordinate.lat >= bottom_right.lat
                                && c.coordinate.lat <= top_left.lat;
                            let is_top = c.coordinate.lat - r <= top_left.lat
                                && c.coordinate.lat - r >= bottom_right.lat
                                && c.coordinate.long >= top_left.long
                                && c.coordinate.long <= bottom_right.long;
                            let is_bottom = c.coordinate.lat + r >= bottom_right.lat
                                && c.coordinate.lat + r <= top_left.lat
                                && c.coordinate.long >= top_left.long
                                && c.coordinate.long <= bottom_right.long;

                            let mut select = is_right || is_left || is_top || is_bottom;

                            // Check it if is in one of the corners
                            select = select
                                || top_left.distance(&c.coordinate) <= r
                                || bottom_left.distance(&c.coordinate) <= r
                                || top_right.distance(&c.coordinate) <= r
                                || bottom_right.distance(&c.coordinate) <= r;

                            select
                        })
                        .cloned()
                        .collect();
                    // Add local coordinates:
                    for c in contained_craters.iter_mut() {
                        let local = crop_model.to_raster_from_latlong(c.coordinate);
                        c.local_raster_coordinates = Some(local);
                    }
                    // Todo
                    if contained_craters.len() == 0 {
                        path.push("no_crater");
                    } else {
                        path.push("with_crater");
                        export.crater_data = Some(contained_craters);
                    }
                    if !path.exists() {
                        fs::create_dir(&path)?;
                    }
                }

                let image = GrayImage::from_vec(
                    config.output_dimensions[0],
                    config.output_dimensions[1],
                    column_vec,
                );
                match image {
                    Some(image_val) => {
                        let img = format!("{}_{}_{}.jpeg", file_name, scaler, counter);
                        path.push(img);
                        if path.exists() {
                            fs::remove_file(&path)?;
                        }
                        let _ = image_val.save(&path);

                        let pretty = ron::ser::PrettyConfig::new().depth_limit(4);
                        // .separate_tuple_members(true)
                        let metadata = ron::ser::to_string_pretty(&export, pretty)
                            .expect("Serialization failed");
                        path.pop();
                        path.push(format!("{}_{}_{}.ron", file_name, scaler, counter));
                        if path.exists() {
                            fs::remove_file(&path)?;
                        }
                        let mut file = fs::File::create(&path)?;
                        file.write_all(metadata.as_bytes())?;

                        path.pop(); // pop file name
                        if crater.is_some() {
                            path.pop(); // pop crater folder
                        }
                    }
                    None => {
                        eprintln!("Could not save image! Line: {}, column: {}", line, column);
                    }
                }
            }
        }
    }
    Ok(())
}

/// Scales images according to either current zoom levels or for all zoom levels
/// defined in the Config. Crops the scaled image according to the fixed output
/// dimensions defined in the Config. The resulting output dimensions will always
/// be the same, just how the source images was scaled before the cropping is
/// configured by the zoom levels.
pub fn crop_image(
    image_data: &ImageData,
    config: &Config,
    zoom_level: Option<u8>,
    training: bool,
    crater: &Option<Vector<CraterData>>,
    export_images: &Option<Vector<bool>>,
    destination_path: Option<&Path>,
) -> Result<(), Box<dyn std::error::Error>> {
    // Convert zoom levels into scalers
    let scalers = match zoom_level {
        Some(zoom_level) => {
            if zoom_level >= config.zoom_levels {
                return Err(Box::new(error::ConfigurationError::with_description(
                    String::from("Chosen Zoom level outside of available zoom level bounds"),
                )));
            }
            vec![get_zoom_fraction(&config, zoom_level)]
        }
        None => {
            let mut scalers;
            if config.zoom_levels == 1 {
                scalers = vec![get_zoom_fraction(&config, 0)];
            } else {
                scalers = Vec::with_capacity(config.zoom_levels as usize);
                for zoom in 0..config.zoom_levels - 1 {
                    scalers.push(get_zoom_fraction(&config, zoom));
                }
            }
            scalers
        }
    };

    // If no destination path is set, use current dir
    let mut path = match destination_path {
        Some(p) => PathBuf::from(p),
        None => PathBuf::from(""),
    };
    // Append EXPORT dir
    path.push(BASE_PATH);
    if !path.exists() {
        std::fs::create_dir(&path)?;
    }

    // Create TRAINING or INFERENCE dir
    if training {
        path.push("training");
        if !path.exists() {
            std::fs::create_dir(&path)?;
        }
    } else {
        path.push("inference");
        if !path.exists() {
            std::fs::create_dir(&path)?;
        }
    }

    let file_name = &image_data.name;
    path.push(file_name.clone());
    if !path.exists() {
        std::fs::create_dir(&path)?;
    }

    for scaler in scalers {
        let dst = image_data.resize(scaler)?;
        save_crops(
            &mut path,
            &config,
            dst.pixels,
            [dst.x_pixels, dst.y_pixels],
            dst.model,
            scaler,
            file_name,
            crater,
            export_images,
        )?;
    }

    Ok(())
}

/// Calculate the zoom fraction from a config and the selected current level
/// wrt. to the number of zoom levels.
fn get_zoom_fraction(config: &Config, zoom_level: u8) -> f64 {
    // If only one zoom return the average between the two
    // Logically max/min should be the same, returning the avg is somewhat of a compromise
    // It will still be correct if min/max are the same value of course.
    if config.zoom_levels == 1 {
        return (config.zoom_fraction_max + config.zoom_fraction_min) / 2.;
    }
    let zoom_fraction =
        (config.zoom_fraction_max - config.zoom_fraction_min) / (config.zoom_levels as f64 - 1.);
    config.zoom_fraction_min + (zoom_level as f64 * zoom_fraction)
}

/// Get vertical and horizontal lines along which the exported images will be cropped
pub fn get_crops(config: &Config, zoom_level: u8, image_size: [f64; 2]) -> CropLines {
    let crop_zoom = get_zoom_fraction(&config, zoom_level);

    let columns = (image_size[0] / (config.output_dimensions[0] as f64 / crop_zoom)) as u32;
    let rows = (image_size[1] / (config.output_dimensions[1] as f64 / crop_zoom)) as u32;

    let mut v_lines = Vector::new();
    let mut h_lines = Vector::new();

    for i in 0..rows + 1 {
        h_lines.push_back((i as f64 * (config.output_dimensions[1] as f64 / crop_zoom)) as u32);
    }
    for i in 0..columns + 1 {
        v_lines.push_back((i as f64 * (config.output_dimensions[0] as f64 / crop_zoom)) as u32);
    }

    CropLines::new(h_lines, v_lines)
}

/// Export all images for a given source directory
pub fn auto_export(
    config: &Config,
    src: &Path,
    dest: &Path,
) -> Result<(), Box<dyn std::error::Error>> {
    let is_tiff = |filename: &Path| -> bool {
        let ext = filename.extension().and_then(OsStr::to_str);
        if let Some(ext) = ext {
            if ext.eq("tiff") {
                return true;
            }
        }
        return false;
    };

    let save_image_crops = |dir: DirEntry| {
        let img = ImageData::from_data_tiff(PathBuf::from(dir.path()));
        if let Ok(img) = img {
            let _ = crop_image(&img, config, None, false, &None, &None, Some(dest));
        }
    };

    let now = Instant::now();

    let file_list = WalkDir::new(src)
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| is_tiff(e.path()))
        .collect::<Vec<DirEntry>>();

    file_list
        .into_par_iter()
        .map(save_image_crops)
        .collect::<()>();
    println!("Done! Took {:?} min", now.elapsed().as_secs() as f64 / 60.);
    Ok(())
}
