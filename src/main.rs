use std::env;
use std::io;
use std::path::Path;
use utils::tools::{read_config, Config};

mod error;
mod gui;
mod image;
mod labels;
mod processing;
mod utils;

const VERSION: &'static str = env!("CARGO_PKG_VERSION");
const HELP_STRG: &str = "CELESTE - planetary imaging.


Options:
    -h, --help              Print this help message
    -c, --config            Provide a config file
    -d, --dst               Destination directory for exports
    -v, --version           Show current version

Arguments:
    gui         Start the graphical admin interface
    export      Export image crops

Examples:
    # Start graphical user interface with a config file
    celeste gui -c ./my_config_file.toml
    
    # Export all files in the source directory according to the given config 
    # and save in the given destination dir
    celeste export -c ./my_config.toml -d ../output_dir/ export ../../image_tile_dir/";

const EXPORT_STR: &str = "CELESTE image export


Usage
    $ celeste export [PATH]

Options:
    -h, --help              print this help message
    -c, --config            provide a config file
    -d, --dst               destination directory for exports
    -y                      do not prompt for confirmation when exporting

Arguments
    PATH     Source directory containing images to be cropped

Examples:
    # Export all files in the source directory according to the given config 
    # and save in the given destination dir
    celeste export -c ./my_config.toml -d ../output_dir/ export ../../image_tile_dir/";

pub fn main() {
    let mut start_gui = false;
    let mut src_dir = None;
    let mut dst_dir = Path::new("");
    let mut config_file = Config::default();
    let mut export = false;
    let mut export_yes_option = false;

    let args: Vec<String> = env::args().collect();

    // CASE: No arguments provided
    if args.len() == 1 {
        println!("{}", HELP_STRG);
        return;
    }

    println!("args: {:?}", args);
    for i in 1..(args.len()) {
        if args[i].eq("-v") || args[i].eq("--version") {
            println!("CELESTE {}", VERSION);
            return;
        } else if args[i].eq("-h") || args[i].eq("--help") {
            println!("{}", HELP_STRG);
            return;
        }
        if args[i].eq("gui") {
            start_gui = true;
        } else if args[i].eq("-c") || args[i].eq("--config") {
            if i + 1 < args.len() {
                match read_config(&args[i + 1]) {
                    Ok(val) => config_file = val,
                    Err(_) => (),
                }
            } else {
                eprintln!("No path provided for configuration file.");
                return;
            }
        } else if args[i].eq("-d") || args[i].eq("--dst") {
            if i + 1 < args.len() {
                dst_dir = Path::new(&args[i + 1]);
            } else {
                eprintln!("No destination path given.");
                return;
            }
        } else if args[i].eq("-y") {
            export_yes_option = true;
        } else if args[i].eq("export") {
            export = true;
            if i + 1 < args.len() {
                src_dir = Some(Path::new(&args[i + 1]));
            } else {
                println!("{}", EXPORT_STR);
                return;
            }
        }
    }

    // Take care of logical errors
    if export && start_gui {
        eprintln!("Cannot start GUI and export. You can export from the GUI but don't use the export flag as well");
    }
    if export && src_dir.is_none() {
        eprintln!("Please provide a source path for exporting.");
    }

    if !(export || start_gui) {
        eprintln!("No option given, please choose between export and gui");
        return;
    }

    if start_gui {
        gui::admin_gui::start_admin_gui(config_file);
    } else if export && src_dir.is_some() {
        if !export_yes_option {
            let out_str = format!(
                "Warning! This command can result in a large amount of files
being saved to disk and possible overwhelm your disk space.
Please make sure these options are correctly set and that you know
what you are doing:

configuration:      {:?},
source:             {:?},
destination:        {:?},
                     
Is this ok [Y/n]:",
                &config_file, src_dir, dst_dir
            );

            let mut input = String::new();
            while !(input.eq("Y\n") || input.eq("n\n")) {
                input.clear();
                println!("{}", out_str);
                io::stdin()
                    .read_line(&mut input)
                    .expect("error: unable to read user input");
            }
            if input.starts_with("n\n") {
                println!("Thank you for trying out CELESTE. See you soon.");
                return;
            }
        }

        // Continue
        let _ = processing::auto_export(&&config_file, &src_dir.unwrap(), &dst_dir);
    }
}
