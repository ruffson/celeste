# CELESTE

![](./assets/logo/celeste_logo_1.png)

# Overview

CELESTE is a tool to inspect GeoTIFF files and crater data. CELESTE can also be used to generate labeled images that can be used for machine learning tasks. Check out the export section to learn more about the output format (labels).

### Supported image types

GeoTIFF types are supported which are saved as strip-based TIFFs due to a limitation of the state-of-the-art Rust TIFF image framework tracked [here](https://github.com/image-rs/image-tiff/issues/116).

To convert a GeoTIFF from a tile-based to a stripe-based image, the following [GDAL](https://gdal.org/) command can be used:

```bash
gdal_translate -co TILED=NO input.tiff  output.tiff
```

LZW (lossless) and Jpeg (lossy) compression are both supported. However, when dealing with large amounts of images, it can be useful to convert the images from a LZW compression to JPEG compression, by adding `-co COMPRESS=JPEG` to the above command. 

GeoTIFFs must end either in `tif`, `tiff`, `TIFF` or `TIF`. It is not possible to load normal TIFFs without the geo tags.

### Supported crater data types

- `.diam` files, __tab (\t) separated__ with one of the following structure: `diameter fraction lon lat` or `diameter fraction lon lat topo_scale_factor` whereas the diameter is given in __km__.
- `.tsv` files,  __tab (\t) separated__ following [Robbins'](https://astrogeology.usgs.gov/search/map/Mars/Research/Craters/RobbinsCraterDatabase_20120821) catalog format with `crater_id lat long diameter secondary` at positions `0, 1, 2, 5, 44` respectively. Diameter also given in __km__. Secondary is a boolean (`Y` or `N`) which can flag a crater as a secondary crater caused by a primary impact.
- `.csv` files, __semicolon (;) separated__ which contains values with the following structure: `crater_id latitude_deg longitude_deg diameter_m latitude_orig_deg longitude_orig_deg updated` whereas `updated ` is optional and is set if the crater was changed via CELESTE in which case the long/lat coordinates are adjusted and the original are kept as they were. If not updated, both coordinates are the same.

## Usage

See installation instructions below. Celeste has a text-based (CLI) and a graphical (GUI) user interface. Either mode can be started via executing the program from the command line. Either by running directly with `cargo` like so: `cargo run --release` or by first building it with `cargo build --release` and then running the executable from `target/release/celeste`. Running without any flags or commands will show the help.

To supply command line arguments with `cargo run` one must add `--` e.g. like this: `cargo run --release -- -h`.

Update the `geo` repository if there are problems due to updates in the [geo](https://gitlab.com/ruffson/geo) repository with `cargo update -p geo`. That repository is mainly concerned with parsing GeoTIFF files beyond the basic implementation in the `image-tiff` crate as discussed [here](https://github.com/image-rs/image-tiff/issues/98#issuecomment-723858446).

### GUI

To start the GUI, execute CELESTE with the `gui` argument: `cargo run --release -- gui`.

You will be presented with an empty window. To load a GeoTIFF, press the LOAD button. This will parse the supported geo tags and show metadata in the bottom left corner: current zoom level, cursor coordinates in meters and long/lat, whether all or only primary craters are shown (_ALL_, or _PRM_) and the current mode. It also attempts to parse the region name if the image name follows [Murray's lab's CTX mosaic](http://murray-lab.caltech.edu/CTX/index.html) naming scheme.

 To load a crater data file, choose _File -> Open Crater File_. After loading a valid crater file, primary craters will be shown in red, secondaries in green.

You can use the mouse to pan around (drag and move) and zoom in and out of the image.

![Overview](./assets/demo_videos/1_overview.gif)

Due to projection errors in the crater data, some craters might be misaligned. To correct for this, CELESTE allows to change the center point of the craters. To do that, select _Edit Crater_ or press the __e__ key, hover with the cursor over a crater and drag-and-move the crater around. The bottom bar will move from the _NAV_ mode into the _EDIT_ mode. When you are done, either press the __ESC__ key, the __e__ key again or use the mouse and select __Done__ from the button bar. You can also switch directly between modes by clicking on them or pressing the keys indicated with square brackets. A crater which was edited will get a blue outline.

![Edit craters](./assets/demo_videos/2_edit_crater.gif)

To export tiles, first display the grid which visualizes the image crops of the exports via _View -> Toggle image crops_. Then, you can select the craters which you want to include in your export data as well as the tile which should be exported. If tiles are selected which do not contain an explicitly selected crater, this will export the images without labels functioning as background (negative) examples.

![Export craters](./assets/demo_videos/3_export.gif)

You can use the `+` and `-` buttons in the top right corner to change the size of tiles. You can modify the min and max tile size as well as the steps in-between in the text fields in the top left. To select all craters choose _Select -> Select all craters_. This menu also offers to deselect all craters and to select all tiles to export. Only tiles are exported which are explicitly selected! When you are done, choose one of the options from the _Image Export_. More on Exporting below.

![Export all craters & change tile size](./assets/demo_videos/4_export.gif)

If a crater data file was loaded which contains score data per crater, e.g. from the output of machine learning inference, the score slider can be used to adjust the confidence score of the craters. This means, that the value of the score defines the confidence that the craters are correctly detected. A higher score means less craters are identified but may be more accurate.

![Crater scores](./assets/demo_videos/5_scores.gif)

### CLI

The main use of the CLI interface is to slice up a GeoTIFF into several smaller slices as JPEG images together with metadata files (see _sidecars_ below). These can be used during inference when doing machine learning and the model allows for larger input images. All that is needed for the conversion is a source directory with the large GeoTiff images and a config file specifying the preferences of output files. More on config files below. More info about the exact usage can be found by running `cargo run --release -- export -h`.

### Export

Whenever images are exported, the exported JPEG will be accompanied by _sidecar_ files. These files are needed to preserve the metadata originally included in GEO tags of the GeoTIFF files, as well as include crater information if present and selected.

#### Sidecar files

These sidecar files are saved one per image and have the same name as the exported image. They are saved in the [Rusty Object Notation (RON)](https://github.com/ron-rs/ron)) format. A human-readable Rust-first format natively supported by the [serde](https://serde.rs/) de-/serializer.

An example for an image sidecar which does not contain a crater:

```rust
(
    orig_name: "E178_N06",
    model: (
        radius_m: 3396190,
        pixel_size: (5, 5, 0),
        model_tie_points: [
            ((
                x: 4736,
                y: 0,
                z: 0,
            ), (
                i: 10574572.5,
                j: 474200,
                k: 0,
            )),
        ],
    ),
    scaler: 1,
    crater_data: None,
)
```

As can be seen the `crater_data` is `None` here because it contains no craters. Sidecar files have the same structure no matter if craters are present or not. An example with craters present:

```rust
(
    orig_name: "E044_N04",
    model: (
        radius_m: 3396190,
        pixel_size: (14.51771653543307, 14.51771653543307, 0),
        model_tie_points: [
            ((
                x: 5888,
                y: 7424,
                z: 0,
            ), (
                i: 2693565.3149606297,
                j: 247870.97244094487,
                k: 0,
            )),
        ],
    ),
    scaler: 0.34440677966101696,
    crater_data: Some([
        (
            crater_id: "10-0-02260",
            coordinate: (
                lat: 4.163031468740011,
                long: 45.46144887474199,
            ),
            diameter_m: 2630,
            coordinate_original: (
                lat: 4.167,
                long: 45.463,
            ),
            secondary: false,
            updated: true,
            selected: true,
            local_raster_coordinates: Some((79.09756731706227, 76.35783138965888)),
            prediction_score: None,
        ),
        (
            crater_id: "10-1-10431",
            coordinate: (
                lat: 4.133544808668528,
                long: 45.47745629880293,
            ),
            diameter_m: 1430,
            coordinate_original: (
                lat: 4.131,
                long: 45.481,
            ),
            secondary: false,
            updated: true,
            selected: true,
            local_raster_coordinates: Some((144.45462378139442, 196.74955087556302)),
            prediction_score: None,
        ),
    ]),
)
```

An ID might not always be available in the original crater data and is auto-generated if not present. Otherwise the data fields for each crater correspond with the fields of `.csv` crater files explained above. The only difference is `local_raster_coordinates` which holds the origin of the crater in local image coordinates.

### Config files

Config files in the `TOML` file format are used to configure Celeste's GUI directly for convenience or for headless slicing-operation. The path of the confg file can be specified via the `-c` flag in either `gui` or `export` (CLI) mode. If no config is specified a default config will be used. An example config (this case with default values) looks like this:

```toml
title = "Celeste export config"
version = "0.1"
output_dimensions = [256, 256]
zoom_levels = 9
zoom_fraction_min = 0.05
zoom_fraction_max = 1
```

Most of these settings are adjusted with the text fields on the top right corner of the GUI interface. If you want to save the current configuration, you can do so by clicking on _File -> Export Config_. In the main directory of this repository, two config files are supplied which have been used for training and inference: `celeste_config_inference.toml` and  `celeste_config_training.toml`.

The inference config looks like this:

```toml
title = "Celeste export config"
version = "0.1"
output_dimensions = [4736, 4736]
zoom_levels = 1
zoom_fraction_min = 1
zoom_fraction_max = 1
```

This was used to split up every image into smaller jpeg chunks at 100% zoom level for inference later. Only whole tiles are exported. In this case the original image has a resolution of 23710x23710 pixels. With the above output dimensions the result will be 5x5 = 25 images, covering a region of 5*4736 = 23680 pixels in each direction which only loses 30 pixels in each direction.

# Installation

[Install Rust](https://www.rust-lang.org/tools/install) if you haven't done so yet.

#### About support on non-Linux platforms

One word of warning, currently this has only been tested on Linux and the way things currently are, I am using a [fork](https://github.com/ruffson/piet/tree/fixed_celeste_grayscale) of the underlying GUI library _piet_ due to an inefficient way it uses to deal with grayscale data: Currently, it stores each grayscale pixel value in a RGB vector, trippling the size. Since the files we deal with already contain a lot of information (~500 mega pixels) the ~500MB required in grayscale 8-bit vector form, becomes over 1.5GB.  This issue was discussed [here](https://github.com/linebender/piet/issues/313), [here](https://github.com/linebender/piet/pull/350) and a solution was proposed by me [here](https://github.com/linebender/piet/pull/360#pullrequestreview-544432471). There are reasons why I fixed it for Linux and not for macOS. In short: The Linux framework uses Cairo which does not have native grayscale support. Instead they only have alpha support. So to efficiently display grayscale images, the actual grayscale value has to be negated and a white background has to be present. However since the other platforms handle this differently, other solutions are needed and due to lack of time and available resources, things are the way they are. Sorry. If there is a need for other platforms, I will have a look. WASM is something I want to explore with more effort soon.

## Prerequisites

Some libraries are necessary to build the whole thing.

### On Fedora

```
sudo dnf install gtk3-devel glib2-devel
```

### Ubuntu

```
sudo apt-get install libgtk-3-dev
```

### macOS (not tested)

- After installing rust make sure to setup your shell with
`source $HOME/.cargo/env` in the terminal that you would like to use to build and run celeste. You can put this line into your shells' setup file to have the rust tools available every time you open your shell.
- Install XCode from the app store
- Open XCode, agree to license and let it setup
~~Install command line tools from the terminal with `xcode-select --install`~~ (This does not seem to be needed anymore as XCode installs the command line tools by itself, at least on Catalina.)

## Building/Running

Build with:

```bash
cargo build --release
```

And run the executible in the target directory like this:

```bash
./target/release/celeste
```

Or directly build and run it with `cargo`:

```bash
cargo run --release
```



it is important to supply the `--release ` flag, otherwise the build will take forever due to how things are in the `image-tiff` crate.