# Changelog

## [UNRELEASED]

## [0.5.1]
### Added
- Updated Readme with more info
- Change export crop levels with keys: '-' (for zooming out) and '=' (for zooming in)
### Changed
- Slider for predicition score instead of textbox

## [0.5.0]
### Added
- Interactive CLI for non-gui operations
- Added destination director for export (not currently available to the GUI)
- Auto-crop CLI: Given a config and a source directory, CELESTE will crop all images in the directory, generate side-car files and save everthing in an optional destination directory (or the default directory) in parallel.

### Changed
- In order to start the gui, a 'gui' option has to be provided to the CLI
- Big refactor to separate gui from image processing

## [0.4.1]
### Added
- Added keyboard shortcuts for selecting, editing craters and exporting crops

## [0.4.0]
### Added
- Allow to select images for export
- Only export training images which have been exported
- Added support for inference export (without label data)
- Parse the region information from the filepath and show in image and save in the exported stuff

### Changed
- Changed the folder structure and image naming, all images now contain the name of the region
- No more folders for the scale of the current export
- For training: craters are only exported if 1. they are selected and 2. if the crater area intersects with the export crop in any way. E.g. even if the crater's centroid is outside of the crop. The local coordinates can thus be outside of the bounds of output dimensions or even negative.

## [0.3.0]
### Added
- Much better performance when zooming out, due to lower res image being saved and swapped out when zooming out far enough (currently 30%)
- Show current zoom level of image crops in upper right corner
- Add menu items for image crop exports
- Resize, crop and save image crops to disk
- For each image, write sidecar file with metadata
- Split up images depending on whether they have craters

### Changed
- Start with finest zoom level

## [0.2.5]
### Added
- Config file to set up celeste which can be edited and exported by the admin gui
- Dragging now makes the cursor into an open hand for a more immersive experience
- Display image crops via menu: `View-Toggle Image Crops`
- Show image crops and allow to change configuration (min/max zoom and number of zoom levels)
- Export image cropping config

### Changed
- Update dependencies: piet, druid
- Increase default window size

## [0.2.0]
### Added
- Viewer mode: in the status bar the current mode is not visible (crater edit or navigation)
- .diam crater file support
- Selection mode: Select craters for later usage in the training phase
- Allow for selection/deselection of all craters via new menu item

### Changed
- Allow zoom to 200%
- When exporting crater data, all entries which have been selected will be exported. Previously all 'updated' (e.g. corrected) craters were exported
